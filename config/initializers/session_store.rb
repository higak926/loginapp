LoginApp::Application.config.session_store :redis_store, {
  servers: [
    {
      host: "redis",
      port: 6379,
      db: 0,
      namespace: "session"
    },
  ],
  # 設定時間
  expire_after: 1.minute,
  key: "_#{Rails.application.class.parent_name.downcase}_session"
}
